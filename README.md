Workbench - A Sturdy 48" x 32" Engineer's Workbench
===================================================
Copyright Ben Wang, 2017

See https://gitlab.com/benwang/Workbench2017/-/blob/master/Print/48-32-2X4-36.pdf for schematic

I designed this before owning any miter or table saws, so the worker at Lowes was kind enough to do all the cuts for me. All you need to build this workbench is:
- (1) 4' x 8' x 3/4" MDF board
- (6) 2" x 4" x 8' dimensional lumber
- 4 locking casters (optional, but highly recommended)
- A pocket jig and a bunch of screws

Software:
- CAD model in FreeCAD